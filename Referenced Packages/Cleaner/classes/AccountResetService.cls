/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class AccountResetService implements Database.Batchable<SObject> {
    global AccountResetService() {

    }
    global void execute(Database.BatchableContext BC, List<SObject> BatchAccounts) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
