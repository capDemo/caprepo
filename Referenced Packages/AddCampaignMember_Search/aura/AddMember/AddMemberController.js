({
	addmember : function(component, event, helper) {
        
        //Navigates to another component by passing camapign ID
        
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef: "cmadd:AddMemberSearch",
            componentAttributes: {
                programId: component.get("v.recordId")
            }
            
        });
    	evt.fire();  
		
	}
})