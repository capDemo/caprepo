({
//Initialization Method which determines the campaign
	doInit : function(component, event, helper) {
         
        
		var divss = component.find('editDel');
		
		var target = component.find('statussection1');  
        $A.util.addClass(target, 'hideme');
		
	
        var previousbtn = component.find('prevbtn');  
        $A.util.addClass(previousbtn, 'mechange');
		
		component.set("v.LeadEnable", true);
		component.set("v.Conatctenable", false);
		
		var all = component.find('SelectingEach');
			all.set("v.disabled","true");
		
        
            
		var m ='Lead';
        var action = component.get("c.getrecord");         
                    
		action.setParams({

			recordId : component.get("v.programId"),
			label : m

		});

		action.setCallback(this, function(a) {

			if (a.getState() === "SUCCESS") {                
                component.set("v.wrecord", a.getReturnValue());
				
			} else if (a.getState() === "ERROR") {

				$A.log("Errors", a.getError());
			}

		});
		$A.enqueueAction(action);
       
        	
	},
	
	//Get records of Camapign Member if lead is selected
    
    callead : function (component, event, helper) {
    	component.find("r1").set("v.value",false); 
    	component.find("r0").set("v.value",true); 
		var cmpSource = event.getSource();
	
		
		var action = component.get("c.getrecord");         
                    
		action.setParams({

			recordId : component.get("v.programId"),
			label :	cmpSource.get("v.label")

		});

		action.setCallback(this, function(a) {

			if (a.getState() === "SUCCESS") {                
                component.set("v.wrecord", a.getReturnValue());
				
			} else if (a.getState() === "ERROR") {

				$A.log("Errors", a.getError());
			}

		});
		$A.enqueueAction(action);
        	
	},
	
	//Get records of Camapign Member if contact is selected
	
 	callcontact : function (component, event, helper) {
    	component.find("r1").set("v.value",true); 
    	component.find("r0").set("v.value",false); 
		var cmpSource = event.getSource();
		//console.log('cmpSource---'+cmpSource);
		var label = cmpSource.get("v.label");
		
	
		var action = component.get("c.getrecord");         
                    
		action.setParams({

			recordId : component.get("v.programId"),
			label :	cmpSource.get("v.label")

		});

		action.setCallback(this, function(a) {

			if (a.getState() === "SUCCESS") {                
                component.set("v.wrecord", a.getReturnValue());
				
			} else if (a.getState() === "ERROR") {

				$A.log("Errors", a.getError());
			}

		});
		$A.enqueueAction(action);
        	
	},
	
	//Get the value of possible Status that can be allocated to a campaign member
	
	hidestaus :  function (component, event, helper){
		
		var shuff  = component.get("v.shuffle");
        console.log('shuffle : '+shuff);
        component.set("v.AddWithStatusDIV", !shuff);
        
        component.set("v.shuffle", !shuff);
               	
		var boolean  = component.get("v.toggle");
		/*
		var cmpTarget = component.find('statussection');
        $A.util.toggleClass(cmpTarget, 'hideme');
		*/
		if(boolean!=true){
			component.set("v.toggle",true);
			var action = component.get("c.getstatus");
			
			action.setParams({

						recordId : component.get("v.programId"),
					});
					
			action.setCallback(this, function(a) {

				if (a.getState() === "SUCCESS") {	
					//console.log('values--'+a.getReturnValue());  
                    component.set("v.statuslist",a.getReturnValue());
					
				}else if (a.getState() === "ERROR") {
					$A.log("Errors", a.getError());
				}
			});

			$A.enqueueAction(action);
		}	
	},
    testclick : function (component, event, helper){
       // console.log('ttestclick----');
        
    },
	
	//Executed when select all checkbox is checked or unchecked
	
	selectall : function (component, event, helper){

        var cbc=component.get("v.checkboxclick");
		console.log('count checkboxclick---'+cbc);
		
        console.log('programIdIN selectallMorning'+component.get("v.programId"));
        console.log('recordId'+component.get("v.recordsIDs"));
        
		if(component.get("v.checkboxclick") >=0)
		{
			var count = component.get("v.checkboxclick");
			count =count+1;
			component.set("v.checkboxclick",count);
			
			if((component.get("v.checkboxclick"))%2!=0)
			{	 //cboxList= [];
				var cboxList = component.find('cbox');                
				console.log('cboxList---'+cboxList+'length--'+cboxList.length);
                var complength;
                if(cboxList != undefined ){
                    if(cboxList.length == undefined)
                        complength = 1;
                    else
                        complength = cboxList.length;
                }
                console.log('complength!!!'+complength);
				for(var i = 0;i<complength;i++)
				{
                    console.log('enter!!!');
                    if(cboxList.length == undefined)
                    	cboxList.set("v.value",true); 
                    else{
                        console.log('cboxList---'+cboxList+'length--'+cboxList.length);
						cboxList[i].set("v.value",true);
                    }
                    
					//console.log(cboxList[i].get("v.text"));
					//helper.selectingallIDs(component, event);
					
					var Selected ;
                    
                    if(cboxList.length == undefined)
                    	Selected = cboxList.get("v.text");
                    else
						Selected = cboxList[i].get("v.text");
                    
					//console.log('Selected-----'+Selected);
					var array = Selected.split('+');
					var SelectedID = array[0];
					var index = array[1];
				
					//console.log('SelectedID---'+SelectedID);
					var IDarray = component.get("v.recordsIDs");
						
					IDarray.push(SelectedID);
					
					//console.log('SelectedID---'+SelectedID+'IDarray---'+IDarray+'index----'+index);
				}
					
			}
			
			else
			{
				var cboxList = component.find('cbox');
                var complength;
                if(cboxList != undefined ){
                    if(cboxList.length == undefined)
                        complength = 1;
                    else
                        complength = cboxList.length;
                }
				//console.log('cboxList---'+cboxList+'length--'+cboxList.length);
                for(var i = 0;i<complength;i++){
                    
                    if(cboxList.length == undefined)
                        cboxList.set("v.value",false);
                    else
                        cboxList[i].set("v.value",false);
                   
                }
                component.set("v.recordsIDs",'');
			}
		}
		
		else
		{
			//console.log('No record present1');
			//$A.get('e.force:refreshView').fire();
			var all = component.find('SelectingEach');
			all.set("v.disabled","true");
            component.set("v.checkboxclick",cbc);
		}
		console.log('programId selectallEvening'+component.get("v.programId"));
        console.log('recordId'+component.get("v.recordsIDs"));
        
    },
	
	//When a particular status is selected from drop down list
	//creates a camapign member(s) for the campaign
	
    ADDwithstatus : function (component, event, helper){
		
        //console.log('programId'+component.get("v.programId"));
        //console.log('recordId'+component.get("v.recordsIDs"));
        component.set("v.counter",false);
		var target = component.find('statussection1');  
        $A.util.addClass(target, 'hideme');
		
		var target = component.find('statussection1');  
        $A.util.addClass(target, 'hideme');
		
		
		var b1 = component.find("r0").get("v.value");
		var b2 =   component.find("r1").get("v.value");
        var selected;
        if(b1 == true)
            selected = 'Lead';
        else
            if(b2 == true)
                selected = 'Contact';
        var x = component.get("v.programId");
        var y = component.get("v.recordsIDs");
		
		var cmpSource = event.getSource();
        var selectedstatus = cmpSource.get("v.value");
		//console.log('selectedstatus----'+selectedstatus);
		
		
		
		var opts = [
            { class: "optionClass", label: "Option1", value: "opt1"},
            { class: "optionClass", label: "Option2", value: "opt2" },
            { class: "optionClass", label: "Option3", value: "opt3" }
           
        ];
		
	
		if(x!='' && y!='')
		{
					component.set("v.WithoutSelecting",false);
					var cmpTarget = component.find('addm');       
					$A.util.removeClass(cmpTarget,'slds-active');
					var cmpTargest = component.find('exist');       
					$A.util.addClass(cmpTargest,'slds-active');
					component.set("v.button1", false);
					
					var target = component.find('statussection1');  
					$A.util.addClass(target, 'hideme');
					
					var action = component.get("c.addwithstatus");

					action.setParams({

						recordId : component.get("v.programId"),
						selectedids : component.get("v.recordsIDs"),
						selectedrecord : selected,
						selectedstatus : selectedstatus

					});
					
					action.setCallback(this, function(a) {

						if (a.getState() === "SUCCESS") {	
							component.set("v.button2", true);
							component.set("v.AddWithStatusDIV",false);
							component.set("v.wrecord", a.getReturnValue());				
							var memberLists = component.get("v.wrecord.existmemList");
							//console.log('memberLists---'+memberLists);
							component.set("v.statusrecords", '');
							component.set("v.statusrecords", memberLists);
							component.set("v.HideFitlers", false);
							
							component.set("v.checkboxclick",0);
							var cboxList1 = component.find('SelectingEach');
							cboxList1.set("v.value",false);
							
							//console.log(component.get("v.statusrecords"));
							
							component.set("v.HideSteps", false);
							component.set("v.prevnext", false);
							
							component.set("v.recordsIDs","")
							
							
						}else if (a.getState() === "ERROR") {
							$A.log("Errors", a.getError());
							var memberLists = component.get("v.wrecord.existmemList");
							
							//console.log('memberLists---'+memberLists);
						
							component.set("v.statusrecords", '');
							component.set("v.statusrecords", memberLists);
							component.set("v.AddWithStatusDIV",false);
						}
					});

					$A.enqueueAction(action);	

		}
		else
		{component.set("v.WithoutSelecting",true);
		component.set("v.AddWithStatusDIV",false);}
        console.log('programIdLast'+component.get("v.programId"));
        console.log('recordIdLast'+component.get("v.recordsIDs"));
    },
    
	//Executed when a particular record is checked or unchecked.
	//Adds or remove record Id to list of records which are to be added
	
    selectrecords : function (component, event, helper){	

		var cmpSource = event.getSource();
        var bol = cmpSource.get("v.value");
   
        var Selected = cmpSource.get("v.text");
		//console.log('Selected-----'+Selected);
		var array = Selected.split('+');
		var SelectedID = array[0];
		var index = array[1];
		

				
				var cboxList = component.find('cbox');
				console.log('cboxList---'+cboxList+'length--'+cboxList.length);
                if(cboxList != undefined){
                    if(cboxList.length == undefined){
                        if (cboxList.get("v.value")==false){
                            var all = component.find('SelectingEach');
                            console.log('SET TO FALSE');
                            all.set("v.value",false);
							var count = component.get("v.checkboxclick");
                            count =0;
                            component.set("v.checkboxclick",count);                            
                        }
                        else{                            
                             var all = component.find('SelectingEach');
                            console.log('SET TO FALSE');
                            all.set("v.value",true);    
                            var count = component.get("v.checkboxclick");
                            count =1;
                            component.set("v.checkboxclick",count);
                            console.log('coutchexk : '+component.get("v.checkboxclick"));
                        }                 
                    }                        
                }
        		
				
				for(var i = 0;i<cboxList.length;i++)
				{	
					//console.log(i+'----------'+cboxList[i].get("v.value"));
					if (cboxList[i].get("v.value")==false)
					{
						console.log(i+'----------'+cboxList[i].get("v.value"));
						
						var all = component.find('SelectingEach');
						console.log('SET TO FALSE');
						all.set("v.value",false);
						var count = component.get("v.checkboxclick");
						count =0;
						component.set("v.checkboxclick",count);
						
                        break;
					}
					else
					{
						var all = component.find('SelectingEach');
						console.log('SET TO TRUE');
						all.set("v.value",true);
						var count = component.get("v.checkboxclick");
						count =1;
						component.set("v.checkboxclick",count);
					}
				}
				
		
        console.log('SelectedID---'+SelectedID);
        var IDarray = component.get("v.recordsIDs");
		
		var IDarray = component.get("v.recordsIDs");
		
		var flag=0;
						
		for( var j=0; j<IDarray.length;j++)
		{	
						
				if (SelectedID == IDarray[j])
				{
					//console.log("Condition Satisfied!");
					flag=1;
					IDarray[j]='';
				}
				
		}
		
		if(flag==0)
		{
			//console.log('Adding');
			if(bol == true)		
				IDarray.push(SelectedID);
			else
				IDarray.splice(index, 1);
		}
		
	 
		//console.log('SelectedID---'+SelectedID+'IDarray---'+IDarray+'index----'+index);
				
    },
	
	//Searches for lead or contact records based on the filters applied
     
    findrecord : function (component, event, helper){
         console.log('asgdgh');
         component.set("v.recordsIDs",'');
        console.log('recordiiiiiiiiiiiiiiiiiiiiiiiii : '+component.get("v.recordsIDs"));
		 // Checking which one is selected- Lead or Contact  
		  component.set("v.statusrecords", '');
          var cboxList=component.find('cbox');
          for(var i = 0;i<cboxList.length;i++){
            cboxList[i].set("v.value","false");
          }                             		 
		  component.set("v.Shifting", true);
			
          component.set("v.WithoutSelecting",false);
		 
		  var b1 = component.find("r0").get("v.value");
	
		  var b2 =   component.find("r1").get("v.value");
		 
		  var selected;
	 
          if(b1 == true)
            selected = 'Lead';
          else
            if(b2 == true)
                selected = 'Contact';
				
        var array =[];
        
        console.log('counter :');
        var selectedop = component.find("operator1").get("v.value");
        var selectedop1 = component.find("operator2").get("v.value");
        var selectedop2 = component.find("operator3").get("v.value");
        var selectedop3 = component.find("operator4").get("v.value");
		
		var f = component.find("field1").get("v.value");
		var f1 = component.find("field2").get("v.value");
		var f2 = component.find("field3").get("v.value");
		var f3 = component.find("field4").get("v.value");
		
		var v = component.find("value1").get("v.value");
		var v1 = component.find("value2").get("v.value");
		var v2 = component.find("value3").get("v.value");
		var v3 = component.find("value4").get("v.value");
		
        console.log('value1 : '+v);
        
		if(f!='' && selectedop!='' && v!=undefined  )
			array.push(f+','+selectedop+','+v);
        
        //console.log('array---'+array);
			
		if(f1!='' && selectedop1!='' && v1!=undefined  )
			array.push(f1+','+selectedop1+','+v1);
		
		if(f2!='' && selectedop2!='' && v2!=undefined  )
			array.push(f2+','+selectedop2+','+v2);
			
		if(f3!='' && selectedop3!='' && v3!=undefined  )
			array.push(f3+','+selectedop3+','+v3);
        
			//GET THE SOURCE OF EVENT 
			
			var cmpSource = event.getSource();
			//console.log('cmpSource---'+cmpSource);
			var label = cmpSource.get("v.label");
			//console.log('label1----'+label);
			if(label == undefined)
				label = cmpSource.get("v.buttonTitle");
			
			
			var x = component.find("r1").get("v.value"); 
			
			//ENABLE COMPANY NAME IN TABLE IF LEAD IS SELECTED
			if(x == true){			
				component.set("v.LeadEnable", false);
				component.set("v.Conatctenable", true);
			}
			
			//ENABLE ACCOUNT NAME IN TABLE IF CONTACT IS SELECTED
			else
				if(x == false){
					component.set("v.LeadEnable", true);
					component.set("v.Conatctenable", false);
				}
		
			
			var offset = component.get("v.offset");
			
			if(label == 'Next'){
				offset =offset +5;
				component.set("v.offset",offset);
								
			}else
			if(label == 'Previous'){
				offset =offset - 5;
				component.set("v.offset",offset);
				
				if(offset==0) {
					var previous = component.find('prevbtn');  
					previous.set("v.disabled","true");
				}
				if(offset>0) {
					var previous = component.find('prevbtn');  
					previous.set("v.disabled","false");
				}
				
					
			}

			var x = component.get("v.programId");

			var action = component.get("c.fetchfilteredrecord");
			action.setParams({

				arary : array,
                selected : selected,
				source : label,
				offset : component.get("v.offset"),
				recordId : component.get("v.programId")

			});

			action.setCallback(this, function(a) {
                console.log('state : '+a.getState());
				if (a.getState() === "SUCCESS") {
				
					var all = component.find('SelectingEach');				
					all.set("v.disabled","true");
				
					console.log('values--'+a.getReturnValue()); 
					//console.log('size------------'+a.getReturnValue().length);
                    if(a.getReturnValue()!=''){
                        if(a.getReturnValue().length==0) 
                        {
                            $A.get('e.force:refreshView').fire();
                        }
                        
                        var all = component.find('SelectingEach');				
						all.set("v.disabled","false");
                        
                        component.set("v.statusrecords", '');
                        component.set("v.statusrecords", a.getReturnValue());
                        
                        
                        component.set("v.IfGoClicked", false);
                         //console.log(component.get("v.IfGoClicked"));
                         component.set("v.prevnext", true);
                          component.set("v.IfGoClickedShow", true);
                         //console.log(component.get("v.IfGoClickedShow"));
                         
                        var m = component.get("v.conrecords");
                        console.log('size------------'+a.getReturnValue().length);
                        //console.log('offset : '+offset);
                        if(offset<a.getReturnValue().length) 
                        {
                            if(a.getReturnValue().length==5)
                            {
                                
                                var next = component.find('nextbtn');  
                                next.set("v.disabled","false");
                            }
                            
                            else
                            {
                                
                                var next = component.find('nextbtn');  
                                next.set("v.disabled","true");
                            }
                        }
                        if(offset>a.getReturnValue().length) {
                    
                        var next = component.find('nextbtn');  
                        next.set("v.disabled","true");
                        }
                        
                        if(a.getReturnValue().length==5)
                            {
                                
                                var next = component.find('nextbtn');  
                                next.set("v.disabled","false");
                            }
                            
                            else
                            {
                                
                                var next = component.find('nextbtn');  
                                next.set("v.disabled","true");
                            }
                        
                        
                        
                        //console.log(offset);
                        
                        if(offset==0) {
                        
                        var next = component.find('prevbtn');  
                        next.set("v.disabled","true");
                        }
                        if(offset>0) {
                    
                        var next = component.find('prevbtn');  
                        next.set("v.disabled","false");
                        }
                    
                    
                        component.set("v.checkboxclick",0);
                        var cboxList1 = component.find('SelectingEach');
                        cboxList1.set("v.value",false);
                        
                        
                        
                        var cboxList = component.find('SelectingEach');
                        cboxList.set("v.value",false);
                        
                        
                        var cboxList = component.find('cbox');
                        console.log('cboxList---'+cboxList+'length--'+cboxList.length);
                        //console.log('Length---'+cboxList.length);
                        
                        for(var i = 0;i<cboxList.length;i++)
                        { 
                        
                            //console.log('ID1---'+cboxList[i].get("v.text"));
                            
                            var Selected = cboxList[i].get("v.text");
                            
                            var array = Selected.split('+');
                            var SelectedID = array[0];
                            //console.log('Selected-----'+SelectedID);
                        
                            var IDarray = component.get("v.recordsIDs");
                            
                            for( var j=0; j<IDarray.length;j++)
                            {	
                            
                                
                                if (SelectedID == IDarray[j])
                                {
                                    //console.log("Condition Satisfied!");
                                    cboxList[i].set("v.value",true);
                                }
                            
                            }
                            
                            
                        }
                        
                        var cboxList = component.find('cbox');
                        //console.log('cboxList---'+cboxList+'length--'+cboxList.length);
                    
                        for(var i = 0;i<cboxList.length;i++)
                        {	
                            
                            if (cboxList[i].get("v.value")==false)
                            {
                                
                                
                                var all = component.find('SelectingEach');
                                
                                all.set("v.value",false);
                                
                                break;
                            }
                            else
                            {
                                var all = component.find('SelectingEach');
                                
                                all.set("v.value",true);
                            }
                        }
                        
                    }
                    else{
                       $A.get('e.force:refreshView').fire(); 
                    }
					
				} else if (a.getState() === "ERROR") {
					//console.log('ERROR!');
					$A.log("Errors", a.getError());
					$A.get('e.force:refreshView').fire();
				}
			});
			$A.enqueueAction(action);	
        console.log('programIdafterGOCLICKFindRecords'+component.get("v.programId"));
        console.log('recordId'+component.get("v.recordsIDs"));
    },
	
	//Searches for existing campaign members of a campaign
   
	findrecord1 : function (component, event, helper){
       
		 // Checking which one is selected- Lead or Contact  
		 component.set("v.recordsIDs",'');
         component.set("v.button2",false);
         console.log('recordiiiiiiiiiiiiiiiiiiiiiiiii : '+component.get("v.recordsIDs"));
		 // Checking which one is selected- Lead or Contact  
		  component.set("v.statusrecords", '');
          var cboxList=component.find('cbox');
          for(var i = 0;i<cboxList.length;i++){
            cboxList[i].set("v.value","false");
          }                             		
        
		  component.set("v.checkboxclick",0);
		 
		  component.set("v.Shifting", true);
        
          component.set("v.ExceptionSelectList",false);
		
        var array =[];
        
        var selectedop = component.find("operator1").get("v.value");
        var selectedop1 = component.find("operator2").get("v.value");
        var selectedop2 = component.find("operator3").get("v.value");
        var selectedop3 = component.find("operator4").get("v.value");
		
		var f = component.find("field1").get("v.value");
		var f1 = component.find("field2").get("v.value");
		var f2 = component.find("field3").get("v.value");
		var f3 = component.find("field4").get("v.value");
		
		var v = component.find("value1").get("v.value");
		var v1 = component.find("value2").get("v.value");
		var v2 = component.find("value3").get("v.value");
		var v3 = component.find("value4").get("v.value");
		
		if(f!='' && selectedop!='' && v!=undefined  )
			array.push(f+','+selectedop+','+v);
        
        //console.log('array---'+array);
			
		if(f1!='' && selectedop1!='' && v1!=undefined  )
			array.push(f1+','+selectedop1+','+v1);
		
		if(f2!='' && selectedop2!='' && v2!=undefined  )
			array.push(f2+','+selectedop2+','+v2);
			
		if(f3!='' && selectedop3!='' && v3!=undefined  )
			array.push(f3+','+selectedop3+','+v3);
        
			
			var action = component.get("c.fetchexistingrecord");
			action.setParams({

				arary : array,
				recordid : component.get("v.programId")

			});

			action.setCallback(this, function(a) {

				if (a.getState() === "SUCCESS") {
				
					var all = component.find('SelectingEach');
					
					all.set("v.disabled","false");
					
					var all = component.find('SelectingEach');
					
					all.set("v.disabled","false");
				
					//console.log('values--'+a.getReturnValue()); 
					//console.log('size------------'+a.getReturnValue().length);
					
				if(a.getReturnValue().length==0) 
					{
						var all = component.find('SelectingEach');
						console.log('Inside');
						all.set("v.disabled","true");
					}
					
				else
				{
					
					component.set("v.statusrecords", '');
					component.set("v.statusrecords", a.getReturnValue());
				
					component.set("v.checkboxclick",0);
					var cboxList1 = component.find('SelectingEach');
					cboxList1.set("v.value",false);
					
					
					var cboxList = component.find('SelectingEach');
					cboxList.set("v.value",false);
					
					var cboxList = component.find('cbox');
					console.log('cboxList---'+cboxList+'length--'+cboxList.length);
					//console.log('Length---'+cboxList.length);
					
					for(var i = 0;i<cboxList.length;i++)
					{ 
						
						//console.log('ID1---'+cboxList[i].get("v.text"));
						
						var Selected = cboxList[i].get("v.text");
						
						var array = Selected.split('+');
						var SelectedID = array[0];
						//console.log('Selected-----'+SelectedID);
					
						var IDarray = component.get("v.recordsIDs");
						
						for( var j=0; j<IDarray.length;j++)
						{	
							
							//console.log('ID2---'+IDarray[j]);
						
							if (SelectedID == IDarray[j])
							{
								//console.log("Condition Satisfied!");
								cboxList[i].set("v.value",true);
							}
						
						}
						
						
					}
					
					var cboxList = component.find('cbox');
					//console.log('cboxList---'+cboxList+'length--'+cboxList.length);
					
					//console.log('Length---'+cboxList.length);
					
					for(var i = 0;i<cboxList.length;i++)
					{	
						//console.log(i+'----------'+cboxList[i].get("v.value"));
						if (cboxList[i].get("v.value")==false)
						{
							//console.log(i+'----------'+cboxList[i].get("v.value"));
							
							var all = component.find('SelectingEach');
							
							all.set("v.value",false);
							
							break;
						}
						else
						{
							var all = component.find('SelectingEach');

							all.set("v.value",true);
						}
					}
					
				}
					
				} else if (a.getState() === "ERROR") {
					//console.log('ERROR!');
					$A.log("Errors", a.getError());
					$A.get('e.force:refreshView').fire();
				}
			});
			$A.enqueueAction(action);	
    },
	
	//Activates the Existing Camapign Member tab
	
	activate : function (component, event, helper){
        
        component.set("v.WithoutSelecting",false);
        var selectedop = component.find("operator1").get("v.value");
        var selectedop1 = component.find("operator2").get("v.value");
        var selectedop2 = component.find("operator3").get("v.value");
        var selectedop3 = component.find("operator4").get("v.value");
        
        var f = component.find("field1").get("v.value");
        var f1 = component.find("field2").get("v.value");
        var f2 = component.find("field3").get("v.value");
        var f3 = component.find("field4").get("v.value");
        
        var v = component.find("value1").get("v.value");
        var v1 = component.find("value2").get("v.value");
        var v2 = component.find("value3").get("v.value");
        var v3 = component.find("value4").get("v.value");
        
        if(f!='' && selectedop!='' && v!=''  ){
            component.find("operator1").set("v.value",'--None--');
            component.find("field1").set("v.value",'--None--');
            component.find("value1").set("v.value",'');
        }
        
        if(f1!='' && selectedop1!='' && v1!=''  ){
            component.find("operator2").set("v.value",'--None--');
            component.find("field2").set("v.value",'--None--');
            component.find("value2").set("v.value",'');
        }
        
        if(f2!='' && selectedop2!='' && v2!=''  ){
            component.find("operator3").set("v.value",'--None--');
            component.find("field3").set("v.value",'--None--');
            component.find("value3").set("v.value",'');
        }
        
        if(f3!='' && selectedop3!='' && v3!=''  ){
            component.find("operator4").set("v.value",'--None--');
            component.find("field4").set("v.value",'--None--');
            component.find("value4").set("v.value",'');
        }
        
		var all = component.find('SelectingEach');
		
		all.set("v.disabled","false");
		component.set("v.checkboxclick",0);
		
		//console.log('Disabled--->'+all.get("v.disabled"));
					
		component.set("v.button1", false);
		
		component.set("v.buttonGO", true);
		
		component.set("v.button2", true);
		
		component.set("v.HideFitlers", true);
		
		component.set("v.HideSteps", false);
		//console.log('Hide---'+ component.get("v.HideSteps"));
		
		var target = component.find('statussection1');  
   
        $A.util.addClass(target, 'hideme');
		
		
	
		var cmpTarget = component.find('addm');       
        $A.util.removeClass(cmpTarget,'slds-active');
		var cmpTargest = component.find('exist');       
        $A.util.addClass(cmpTargest,'slds-active');
		
		var cmp = component.find('addmember'); 					
        $A.util.addClass(cmp, 'hideme'); 
       
		var mList = component.get("v.wrecord.existmemList");
		//console.log('mList----'+mList);
		component.set("v.statusrecords", '');
		component.set("v.statusrecords", mList);
		if(component.get("v.wrecord.existmemList")==''){
            console.log('inside iff');
            component.find('SelectingEach').set("v.disabled","true");
        }	
        else
            console.log('inside elseee');
        
		var action = component.get("c.getstatus");
		
			action.setParams({

						recordId : component.get("v.programId"),
					});
					
			action.setCallback(this, function(a) {

				if (a.getState() === "SUCCESS") {	
					//console.log('values--'+a.getReturnValue());  
                    component.set("v.statuslist",a.getReturnValue());
					
					//var all = component.find("SelectingEach");
					
					//all.set("v.disabled","false");
					component.set("v.checkboxclick",0);
					
				}else if (a.getState() === "ERROR") {
					$A.log("Errors", a.getError());
				}
			});

			$A.enqueueAction(action);
		
		
	},
	
	//Responsible for displaying Edit-Delete Modal
	
	showdiv :  function (component, event, helper){
	
		component.set("v.editdeldp", true);
		
		var divisions  = component.find('editDel');
		
		for(var i = 0;i<divisions.length;i++)
			divisions[i].set("v.title",'edit'+i);
					
		
		var btns  = component.find('btncheck');
		
		for(var i = 0;i<btns.length;i++)
			btns[i].set("v.label",'edit'+i);						
				
		var selectedcomp = event.getSource();
		var selectedlbtn = selectedcomp.get("v.label");
		
		for(var i = 0;i<divisions.length;i++){		
			if(divisions[i].get("v.title") == selectedlbtn )
				$A.util.toggleClass(divisions[i], 'hideme');				
		}
			        
	},
	
	//Activates the Adding New Camapign Member tab
	
	activadd : function (component, event, helper){
        
        $A.get('e.force:refreshView').fire();
	
		if (component.get("v.Shifting"))
		{
			
			component.set("v.buttonGO", false);
			$A.get('e.force:refreshView').fire();

		}
		
		else
		{
			
			component.set("v.button1", true);
			var target = component.find('statussection');       
			$A.util.addClass(target, 'hideme'); 
			
			component.set("v.HideSteps", true);
		//	console.log('Hide---'+ component.get("v.HideSteps"));
			
			component.set("v.button2", false);
			
			var cmp = component.find('exitsmember');  					
			$A.util.addClass(cmp, 'hideme');  
			var cmpTargest = component.find('exist');       
			$A.util.removeClass(cmpTargest,'slds-active');
			component.set("v.srecords", '');
			
			var cmpTarget = component.find('addm');       
			$A.util.addClass(cmpTarget,'slds-active');
			
			var x;
			component.set("v.statusrecords", '');
			component.set("v.statusrecords", x);
			
			component.set("v.buttonGO", false);
		}
		
	},
	
	//Display Edit modal
	
	showeditdiv : function (component, event, helper){
		
		component.set("v.showedit",true);
		
		component.set("v.editdeldp", false);
		
		var selectedcomp = event.getSource();
		component.set("v.MemberID",selectedcomp.get("v.title"));
		
		var action = component.get("c.EditMemberdetails");
		action.setParams({
				memberID : selectedcomp.get("v.title")
		});

		action.setCallback(this, function(a) {

			if (a.getState() === "SUCCESS") {
			
				component.set("v.memberDetail", a.getReturnValue());
				var memdetail = a.getReturnValue().split(',');
			
				component.set("v.detailName",memdetail[0]);
				component.set("v.detailStatus",memdetail[1]);
				component.set("v.memberStatus",memdetail[1]);
				component.set("v.SelectedLC",memdetail[2]);
				var sList = component.get("v.statuslist");
				
			}
			else if (a.getState() === "ERROR") {
			
				$A.log("Errors", a.getError());
			}
		});

		$A.enqueueAction(action);	
		
	},
	
	//Saves the changes submitted to Edit modal
	
	saveMemberRecord :  function (component, event, helper){
		var nowstatus = component.get("v.detailStatus");
		
		var previousstatus = component.get("v.memberStatus");
		//console.log('previousstatus----'+previousstatus);
		
		
		if(nowstatus != previousstatus ){
        
			var action = component.get("c.SaveMemberdetails");
			action.setParams({
					memberID : component.get("v.MemberID"),
					memberStatus : component.get("v.detailStatus")
			});

			action.setCallback(this, function(a) {

				if (a.getState() === "SUCCESS") {
					component.set("v.showedit",false);
					helper.Updaterecords(component);
					
				}
				else if (a.getState() === "ERROR") {
				
					$A.log("Errors", a.getError());
				}
			});

			$A.enqueueAction(action);	
        }
		
	},
	
	//Hides Edit modal
	
	canceledit : function (component, event, helper){
		component.set("v.showedit",false);
	},
	
	//Displays Delete modal
	
	showdeldiv : function (component, event, helper){
		var selectedcomp = event.getSource();
		
				component.set("v.editdeldp", false);

		component.set("v.MemberID",selectedcomp.get("v.title"));
		//console.log('memberid--'+component.get("v.MemberID"));
		component.set("v.showdel",true);
		
	},
	cancelDel : function (component, event, helper){
		component.set("v.showdel",false);
	},
	
	//Saves the changes submitted to Delete modal
	
	removeMemberRecord : function (component, event, helper){
	
		
		//console.log('memberid--'+component.get("v.MemberID"));
		
		var action = component.get("c.removeMember");
		
		action.setParams({
				memberID : component.get("v.MemberID")
		});
		
		action.setCallback(this, function(a) {

			if (a.getState() === "SUCCESS") {
				component.set("v.showdel",false);
				helper.Updaterecords(component);
                if(component.get("v.wrecord.existmemList")==''){
                    console.log('inside iff');
                    component.find('SelectingEach').set("v.disabled","true");
                }	
				
			}
			else if (a.getState() === "ERROR") {
			
				$A.log("Errors", a.getError());
			}
		});

		$A.enqueueAction(action);	
	
	},
	
	//Displays drop down list for Update Status
	
	updateStatus : function (component, event, helper){
	

		/*var cmpTarget = component.find('statussection1');
        $A.util.toggleClass(cmpTarget, 'hideme'); */
        
        var shuffU  = component.get("v.shuffleUpdate");
        console.log('shuffle : '+shuffU);
        component.set("v.statusUpdate", !shuffU);
        
        component.set("v.shuffleUpdate", !shuffU);
		
		//component.set("v.statusUpdate",true);
		
		
	},
	
	//Responsible for updating the status of existing Campaign Member
	
	updatememberStatus  : function (component, event, helper){	        
        
		var cmpSource = event.getSource();
        var selectedstatus = cmpSource.get("v.value");
		//console.log('selectedstatus----'+selectedstatus);
		
		var idsofrecord= component.get("v.recordsIDs");
		//console.log(idsofrecord=='');
		component.set("v.statusUpdate",false);
		
		
		if (idsofrecord!='' && idsofrecord!= null)
		{
				component.set("v.ExceptionSelectList", false);
				var action = component.get("c.updatememberrecord");
				
				action.setParams({

					recordId : component.get("v.programId"),
					selectedids : component.get("v.recordsIDs"),			
					selectedstatus : selectedstatus

				});
				
				action.setCallback(this, function(a) {

					if (a.getState() === "SUCCESS") {	

						var cmpTargest = component.find('statussection1');  

						$A.util.addClass(cmpTargest, 'hideme');
						helper.Updaterecords(component);
						component.set("v.recordsIDs","");
                        console.log('selecting eeeeeeeeach : '+component.find('SelectingEach').get("v.value"));
						component.set("v.checkboxclick",0);
						var cboxList1 = component.find('SelectingEach');
						cboxList1.set("v.value",false);
						
						
					}else if (a.getState() === "ERROR") {
						$A.log("Errors", a.getError());
					}
				});
				$A.enqueueAction(action);
            if(component.get("v.counter")==true){
            	var selectedop = component.find("operator1").get("v.value");
                var selectedop1 = component.find("operator2").get("v.value");
                var selectedop2 = component.find("operator3").get("v.value");
                var selectedop3 = component.find("operator4").get("v.value");
                
                var f = component.find("field1").get("v.value");
                var f1 = component.find("field2").get("v.value");
                var f2 = component.find("field3").get("v.value");
                var f3 = component.find("field4").get("v.value");
                
                var v = component.find("value1").get("v.value");
                var v1 = component.find("value2").get("v.value");
                var v2 = component.find("value3").get("v.value");
                var v3 = component.find("value4").get("v.value");
                
                if(f!='' && selectedop!='' && v!=''  ){
                    component.find("operator1").set("v.value",'--None--');
                    component.find("field1").set("v.value",'--None--');
                    component.find("value1").set("v.value",'');
                }
                        
                if(f1!='' && selectedop1!='' && v1!=''  ){
                    component.find("operator2").set("v.value",'--None--');
                    component.find("field2").set("v.value",'--None--');
                    component.find("value2").set("v.value",'');
                }
                    
                if(f2!='' && selectedop2!='' && v2!=''  ){
                    component.find("operator3").set("v.value",'--None--');
                    component.find("field3").set("v.value",'--None--');
                    component.find("value3").set("v.value",'');
                }
                        
                if(f3!='' && selectedop3!='' && v3!=''  ){
                    component.find("operator4").set("v.value",'--None--');
                    component.find("field4").set("v.value",'--None--');
                    component.find("value4").set("v.value",'');
                }
            }
		}
		
		else
		{
			component.set("v.ExceptionSelectList", true);
			var cmpTargest = component.find('statussection1');  

			$A.util.addClass(cmpTargest, 'hideme');
		}
			
	},
	
	//Responsible for removing existing Campaign Member
	
	removeCampiagnMember : function (component, event, helper){

		console.log('programId'+component.get("v.programId"));
        console.log('recordIddd'+component.get("v.recordsIDs"));     
       
	
		var idsofrecord= component.get("v.recordsIDs");
		//console.log(idsofrecord=='');
		
		if (idsofrecord!='' && idsofrecord!= null)
		{
				component.set("v.ExceptionSelectList", false);
				var action = component.get("c.removememberrecord");
				 
				
				action.setParams({

					recordId : component.get("v.programId"),
					selectedids : component.get("v.recordsIDs")						
				});
				
				action.setCallback(this, function(a) {

					if (a.getState() === "SUCCESS") {	
						
						helper.Updaterecords(component);
						
						component.set("v.checkboxclick",0);
						var cboxList1 = component.find('SelectingEach');
						cboxList1.set("v.value",false);	
                        
					}else if (a.getState() === "ERROR") {
						$A.log("Errors", a.getError());
					} 
				});
            	component.set("v.recordsIDs",'');
            
				$A.enqueueAction(action);
         		
            if(component.get("v.counter")==true){
                var selectedop = component.find("operator1").get("v.value");
                var selectedop1 = component.find("operator2").get("v.value");
                var selectedop2 = component.find("operator3").get("v.value");
                var selectedop3 = component.find("operator4").get("v.value");
                
                var f = component.find("field1").get("v.value");
                var f1 = component.find("field2").get("v.value");
                var f2 = component.find("field3").get("v.value");
                var f3 = component.find("field4").get("v.value");
                
                var v = component.find("value1").get("v.value");
                var v1 = component.find("value2").get("v.value");
                var v2 = component.find("value3").get("v.value");
                var v3 = component.find("value4").get("v.value");
                
                if(f!='' && selectedop!='' && v!=''  ){
                    component.find("operator1").set("v.value",'--None--');
                    component.find("field1").set("v.value",'--None--');
                    component.find("value1").set("v.value",'');
                }
                        
                if(f1!='' && selectedop1!='' && v1!=''  ){
                    component.find("operator2").set("v.value",'--None--');
                    component.find("field2").set("v.value",'--None--');
                    component.find("value2").set("v.value",'');
                }
                    
                if(f2!='' && selectedop2!='' && v2!=''  ){
                    component.find("operator3").set("v.value",'--None--');
                    component.find("field3").set("v.value",'--None--');
                    component.find("value3").set("v.value",'');
                }
                        
                if(f3!='' && selectedop3!='' && v3!=''  ){
                    component.find("operator4").set("v.value",'--None--');
                    component.find("field4").set("v.value",'--None--');
                    component.find("value4").set("v.value",'');
                }
                 
            }
                
		}
		
		else
		{
			component.set("v.ExceptionSelectList", true);
			
		}		   
        
	},
	

    applyCSS: function(cmp, event) {
        var cmpTarget = cmp.find('testdiv');
        $A.util.addClass(cmpTarget, 'mechange');
        
         var previousbtn = cmp.find('prevbtn');  
      //  console.log('prevbtn----'+previousbtn);
        $A.util.addClass(previousbtn, 'mechange');
    }
	
	
})